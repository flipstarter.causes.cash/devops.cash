
# LET'S BU!LD BCH!

__2021 is the year Bitcoin BUILDS!__

Greetings Bitcoiners!

This is __[@Shomari](https://twitter.com/ShomariPrince)__ with a new initiative for Bitcoin's builders.

__TL;DR — Cash DevOps invites ALL 33 teams from this week's CoinParty hackathon to bring your amazing projects to our platform _(under development)_. Starting today, we're providing all the FREE tools, services and support needed for EACH team to launch a Flipstarter campaign in the next 30 days.__

Last week, I had the pleasure of engaging with 30+ of the most talented development teams that exist in all of crypto. But just because the __[BU CoinParty](https://coinparty.org)__ is over doesn't mean we should stop hacking. Cash DevOps is preparing to organize a Flipstarter campaign for each of the 33 CoinParty teams and we need your help.

Creating a Flipstarter campaign isn't as easy as it looks. Besides having an idea and building a team, you also have to adequately sell your vision to a community of would-be supporters. Our team is ready to mentor and support a new wave of builders on their way to Bitcoin greatness.

I created Cash DevOps after a failed attempt last year to add Bitcoin Cash to the Stack Exchange community. Back then, the focus was solely on node development. But after this weeks events, I'm hopeful that focus will now shift to building services that add value to the ecosystem.

__I'd love to see the same efforts made towards onboarding new merchants and users as there is for onboarding new developers and creators.__

I'm proposing a fully-developed platform built by Bitcoiners for Bitcoiners; while welcoming new developers from other communities like Ethereum and Dash.

[![Cash DevOps Screenshot](https://i.imgur.com/cka8SF6.jpg)](https://devops.cash/projects)

##### https://devops.cash/projects

But Rome wasn't built in a day, so this Flipstarter campaign would like to get us to that first milestone as soon as possible. Volunteer funding is working better than anyone could have imagined. Now seems like a great time to step things up and lean into this bull market.

This platform aims to be a home for Bitcoin's builders to curate their projects in a open and dedicated community. This Flipstarter is meant to get that process started by building a home for all 33 of the amazing CoinParty teams to showcase their bright visions.

__My plan comes in 3 short phases:__

## Phase One

Already working on importing all 33 projects into the DevOps NEW "Projects" area.

__This is where teams will be able to:__

1. Showcase their projects
2. Manage resources
3. Coordinate events

## Phase Two

The pace of the CoinParty hackathon was furious, and many teams never really caught their true stride. We aim to slow things down a bit and spend time mentoring teams ready to move their vision forward.

__This phase will allow teams to:__

1. Complete their project docs (bizplan, lean canvas, pitch deck, etc)
2. Build their team members

## Phase Three

Each team will naturally move at a different pace. We'll aim to launch each campaign as quickly as possible on a first come first serve basis.

__At this phase, we will enable each team to:__

1. Launch a Flipstarter campaign hosted FREE on our server
2. Create a social media campaign

# Use of funds

> NOTE: Cash DevOps monthly expense reports offer 100% transparency for all use of funds.

## 1. FREE setup & hosting for all campaigns

No need to get involved with Digital Ocean.
All DevOps campaigns will be setup and hosted on a dedicated Flipstarter Too server.

## 2. Complete development of the Projects area

Cash DevOps has been in development since April of last year as a test bed for the __[NitoJS library.](https://nitojs.org)__
At this time, I would like to complete development of a new area where developers can curate their projects in an environment specifically designed for Bitcoin developers.

##### Accountability is sorely lacking from volunteer funding

Cash DevOps aims to develop a transparent accounting system that conveniently allows campaigns to report their activities and expenditures.

## 3. Flipstarter Too front-end development

We're updating the current Flipstarter front-end to support a new minimum pledge amount of $1.00. There will also exist an option to enter a specific amount (eg. $13.37).

# What can you do?

The activities outlined above are already in motion.
This Flipstarter is meant to not only expedite the process, but to serve as an example for the newly forked Flipstarter Too functionality.
Contributors can now make pledges as low as $1.00.

I'd love to see more community participation in volunteer funding, so I do hope to see lots of $1.00 donations for this initiative.

##### And don't forget the __[Bitcoin.com wallet app](https://wallet.bitcoin.com)__!
Use the #1 Bitcoin mobile wallet to make pledges right from your phone.

Let's all work together to #MakeBitcoinCashAgain!

__Cheers!__  
__Shomari__





---





# Flipstarter Too

##### https://gitlab.com/bchplease/flipstarter-too

__[Bitcoin Please](https://bchplease.org)__ has slightly modified the __["original" Flipstarter](https://gitlab.com/flipstarter/backend)__ code to now allow for smaller individual donations. You will be able to pledge as little as $1.00 to your favorite CoinParty project(s).

__Our team is now working on the user-interface to add support for:__

- Custom donation amounts
- SLP addresses
- Email reminders
- and more...

# 33 CoinParty Projects

In order of CCK tokens received, below is the complete list of CoinParty Block Reward participants:

1. [Tommygun's Coin Slot](https://devpost.com/software/tommygun-s-coin-slot)
2. [swap.cash](https://devpost.com/software/swapcash)
3. [Paytaca](https://devpost.com/software/paytaca)
4. [Unstoppable Web](https://devpost.com/software/bitcoin-apps)
5. [GeoDrop](https://devpost.com/software/purelypeer)
6. [OnlyCoins](https://devpost.com/software/onlycoins)
7. [SLP Ship](https://devpost.com/software/slp-ship)
8. [BitPal](https://devpost.com/software/bitpal)
9. [SplitSeed](https://devpost.com/software/splitseed)
10. [CashBlocks](https://devpost.com/software/cash-blocks)
11. [Hedge Tracker](https://devpost.com/software/hedge-tracker-ctgr0u)
12. [BCH Market Bot](https://devpost.com/software/crypto-bot)
13. [options.cash](https://devpost.com/software/options-cash)
14. [Chimoney](https://devpost.com/software/chimoney)
15. [fridge.cash](https://devpost.com/software/oyster-card)
16. [Revolutionary.Cash](https://devpost.com/software/revolutionary-cash)
17. [SLaP](https://devpost.com/software/slap)
18. [Marzipan](https://devpost.com/software/marzipan)
19. [hCaptcha Telgram Bot](https://devpost.com/software/hcaptcha-telegram-bot)
20. [CoinToys](https://devpost.com/software/cointoys-hbenqj)
21. [DEFI Coinflips](https://devpost.com/software/defi-coin-flips)
22. [CryptoScrum](https://devpost.com/software/cyberscrum)
23. [Pangolin II](https://devpost.com/software/pangolin-ii)
24. [Easy NFTs](https://devpost.com/software/easy-nfts)
25. [Skylar](https://devpost.com/software/skylar)
26. [ChainBridge](https://devpost.com/software/chainbridge)
27. [Blinkit](https://devpost.com/software/blinkit-ez5q19)
28. [Chipotle.cash](https://devpost.com/software/chipotle-cash)
29. [OnlyCash](https://devpost.com/software/onlycash)
30. [Project Unity](https://devpost.com/software/project-unity)
31. [Repo Marketplace](https://devpost.com/software/open-source-repo-marketplace)
32. [InsideParty](https://devpost.com/software/insideparty)
33. [Sagenet](https://devpost.com/software/sagenet)

# Stack Exchange

In April of 2020, I initiated a campaign to bring Bitcoin Cash to Stack Exchange.
However, that attempt was cancelled by the site administrators as a "failed attempt" after just 7 days of inactivity.

The fact is that this campaign succeeded in garnering over 100+ pledges and reaching 38% completion in its first week, so I couldn't comprehend why they would consider it a "failed" effort a week later.

##### on that day is when I registered __devops.cash__

[![Stack Exchange - Closed](https://i.imgur.com/1vuJHSF.jpg)](https://web.archive.org/web/20200408063858/https://area51.stackexchange.com/proposals/123786/bitcoin-cash)

##### [https://area51.stackexchange.com/proposals/123786/bitcoin-cash](https://web.archive.org/web/20200408063858/https://area51.stackexchange.com/proposals/123786/bitcoin-cash)

---

Please feel to reach to me and our team on social media to answer any questions:

Twitter - __[@ShomariPrince](https://twitter.com/ShomariPrince)__

Reddit - __[Cash DevOps](https://www.reddit.com/r/CashDevOps/)__
